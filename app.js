const {Sequelize, QueryTypes} = require('sequelize');

if (process.env.NODE_ENV !== 'production'){
    require('dotenv').config();
}


const sequelize = new Sequelize({
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT
});

async function connectCities(){
    try {
        await sequelize.authenticate();
        const cities = await sequelize.query('SELECT name FROM city', {type: QueryTypes.SELECT} );
        console.log(cities);
        await sequelize.close();
        
        
    }
    catch (e) {
        console.log(e);
        
    }
}

async function connectCountries(){
    try {
        await sequelize.authenticate();
        const countries = await sequelize.query('SELECT name FROM country', {type: QueryTypes.SELECT});
        console.log(countries);
        await sequelize.close();
        
        
    }
    catch (e) {
        console.log(e);
        
    }
}


async function connectLanguage(){
    try {
        await sequelize.authenticate();
        const languages = await sequelize.query('SELECT language FROM countrylanguage', {type: QueryTypes.SELECT});
        console.log(languages);
        await sequelize.close();
        
        
    }
    catch (e) {
        console.log(e);
        
    }
}
